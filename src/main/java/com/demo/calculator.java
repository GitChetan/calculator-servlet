//
//package com.demo;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@WebServlet("/calculator")
//public class calculator extends HttpServlet {
//    private static final long serialVersionUID = 1L;
//
//    public calculator() {
//        super();
//    }
//
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        int num1 = Integer.parseInt(request.getParameter("num1"));
//        int num2 = Integer.parseInt(request.getParameter("num2"));
//        String operation = request.getParameter("operation");
//        int result = 0;
//
//        switch (operation) {
//            case "add":
//                result = num1 + num2;
//                break;
//            case "subtract":
//                result = num1 - num2;
//                break;
//            case "multiply":
//                result = num1 * num2;
//                break;
//            case "divide":
//                if (num2 != 0) {
//                    result = num1 / num2;
//                } else {
//                    response.getWriter().println("Error: Division by zero!");
//                    return;
//                }
//                break;
//            default:
//                response.getWriter().println("Invalid operation!");
//                return;
//        }
//
//        PrintWriter out = response.getWriter();
//        out.println("Result is: " + result);
//    }
//}
//
//

package com.demo;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/calculator")
public class calculator extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public calculator() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int num1 = Integer.parseInt(request.getParameter("num1"));
        int num2 = Integer.parseInt(request.getParameter("num2"));
        String operation = request.getParameter("operation");

        switch (operation) {
            case "add":
                request.setAttribute("num1", num1);
                request.setAttribute("num2", num2);
                request.getRequestDispatcher("/add").forward(request, response);
                break;
            case "subtract":
                request.setAttribute("num1", num1);
                request.setAttribute("num2", num2);
                request.getRequestDispatcher("/subtract").forward(request, response);
                break;
            case "multiply":
                request.setAttribute("num1", num1);
                request.setAttribute("num2", num2);
                request.getRequestDispatcher("/multiply").forward(request, response);
                break;
            case "divide":
                request.setAttribute("num1", num1);
                request.setAttribute("num2", num2);
                request.getRequestDispatcher("/divide").forward(request, response);
                break;
            default:
                response.getWriter().println("Invalid operation!");
                break;
        }
    }
}

