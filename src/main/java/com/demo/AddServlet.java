// AddServlet.java
package com.demo;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/add")
public class AddServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int num1 = Integer.parseInt(request.getAttribute("num1").toString());
        int num2 = Integer.parseInt(request.getAttribute("num2").toString());
        int result = num1 + num2;

        // Set content type
        response.setContentType("text/html");

        // Create HTML response content
        String htmlResponse = "<html><head><title>Result</title><style>body {font-family: Arial, sans-serif; background-color: rgb(216, 204, 232);}" +
                ".container {text-align: center; margin-top: 50px;}" +
                ".result {background-color: #fff; padding: 20px; border-radius: 10px; box-shadow: 0 0 10px rgba(0,0,0,0.1);}" +
                "</style></head><body><div class='container'><div class='result'>" +
                "<h2>Result</h2>" +
                "<p>The result of addition is: " + result + "</p>" +
                "</div></div></body></html>";

        // Write response
        response.getWriter().println(htmlResponse);
    }
}
